---
Title: Annual meeting 2022
---

We would like to invite everyone interested in getting involved to [become
members] of our organization, as well as attending our annual meeting. At
the annual meeting, we will elect a new board, go through the  director's
report and the yearly budget for the organization. We encourage all
members to take part in shaping the future of the organization.

The meeting will be held on the 28th of December, at 20:00 CET, and you can
[participate on Jitsi][jitsi].

[become members]: https://docs.google.com/forms/d/e/1FAIpQLSdTPDVYbh84-KjF_9FwIdFhsmEWA5Ht3LHV5lwslImeV58RWg/viewform?usp=sf_link
[jitsi]: https://meet.jit.si/moderated/032208364d0dd7cab82e53eba01db4d1542db9a4529c6dc0299af88a0ab78b16
