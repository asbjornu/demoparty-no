---
Title: Årsmøte 2022
slug: årsmøte-2022
---

Vi vil gjerne invitere alle som er interessert i å engasjere seg til å
[melde seg inn] i foreningen, samt å stille på vårt årsmøte. På årsmøtet
vil vi diskutere og velge nytt styre, samt gjennomgå årsberetningen og
årsregnskapet for foreningen. Vi oppfordrer alle medlemmer til å delta
og være med på å påvirke fremtiden til foreningen.

Årsmøtet vil avholdes 28. Desember klokken 20:00, og du [kan delta på Jitsi][jitsi].

[melde seg inn]: https://docs.google.com/forms/d/e/1FAIpQLSdTPDVYbh84-KjF_9FwIdFhsmEWA5Ht3LHV5lwslImeV58RWg/viewform?usp=sf_link
[jitsi]: https://meet.jit.si/moderated/032208364d0dd7cab82e53eba01db4d1542db9a4529c6dc0299af88a0ab78b16
