---
Title: Hjem
---

Norsk Demopartyforening er en ideell organisasjon som har som mål å
fremme [demopartyer] i Norge. Mer konkret så arrangerer vi [Black Valley]
demopartyet i Oslo, Norge.

## Kontakt

Du kan nå Norsk Demopartyforening via e-post på
<contact@demoparty.no>. Annen relevant informasjon kan bli funnet i
[Brønnøysundregisterene].

## Vedtekter

Du kan finne våre vedtekter [her]({{< ref "vedtekter.md" >}}).

## Årsmøte 2022

Vi avholder snart årsmøte for 2022, [les mer om det her]({{< ref "annual-meeting-2022.md" >}}).

{{< button url="https://docs.google.com/forms/d/e/1FAIpQLSdTPDVYbh84-KjF_9FwIdFhsmEWA5Ht3LHV5lwslImeV58RWg/viewform?usp=sf_link" >}}
Bli medlem!
{{</ button >}}


[demopartyer]: https://en.wikipedia.org/wiki/Demoscene#Parties
[Black Valley]: https://blackvalley.party/
[Brønnøysundregisterene]: https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=928153193
