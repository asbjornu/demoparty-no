---
Title: Home
---

Norsk Demopartyforening is a non-profit organization that that has the
goal of furthering [demoparties] in Norway. In particular, we arrange the
[Black Valley] party in Oslo, Norway.

## Contact

You can reach Norsk Demopartyforening via email at
<contact@demoparty.no>. Other relevant information can be found in
[Brønnøysundregisterene] (Norwegian language only, sorry).

## Bylaws

You can find our bylaws [here]({{< ref path="vedtekter.md" lang="nb" >}}). (Norwegian language only, sorry).

## Annual meeting 2022

We are soon holding our annual meeting for 2022, [read more about it here]({{< ref "annual-meeting-2022.md" >}}).

{{< button url="https://docs.google.com/forms/d/e/1FAIpQLSdTPDVYbh84-KjF_9FwIdFhsmEWA5Ht3LHV5lwslImeV58RWg/viewform?usp=sf_link" >}}
Become a member!
{{</ button >}}

[demoparties]: https://en.wikipedia.org/wiki/Demoscene#Parties
[Black Valley]: https://blackvalley.party/
[Brønnøysundregisterene]: https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=928153193
